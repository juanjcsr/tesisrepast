/*
ęCopyright 2012 Nick Malleson
This file is part of RepastCity.

RepastCity is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RepastCity is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RepastCity.  If not, see <http://www.gnu.org/licenses/>.
 */

package repastcity3.agent;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import repast.simphony.query.space.gis.GeographyWithin;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.gis.Geography;
import repastcity3.environment.Building;
import repastcity3.environment.Route;
import repastcity3.main.ContextManager;

public class DefaultAgent implements IAgent {

	private static Logger LOGGER = Logger.getLogger(DefaultAgent.class.getName());

	private Building home; // Where the agent lives
	private Building workplace; // Where the agent works
	private Route route; // An object to move the agent around the world

	private boolean goingHome = false; // Whether the agent is going to or from their home

	private static int uniqueID = 0;
	private int id;
	
	private AgentType type;
	private AgentStatus status;
	private ArrayList<DefaultAgent> infectedList;

	public DefaultAgent() {
		this.id = uniqueID++;
		this.infectedList = new ArrayList<DefaultAgent>();
		//Get the random occupation of the Agent
		int type = ContextManager.getUniformRandom(99);
		this.status = AgentStatus.SUSCEPTIBLE;
		//50% of the population will be workers
		if (type < 50) {
			
			this.type = AgentType.WORKER;
			// Find a building that agents can use as their workplace. First, iterate over all buildings in the model
	        for (Building b : ContextManager.buildingContext.getRandomObjects(Building.class, 10000)) {
	                // See if the building is a bank (they will have type==2).
	                if (b.getType()==2) {
	                        this.workplace = b;
	                        break; // Have found a bank, stop searching.
	                }
	        }
		}
		//free roaming agents
		else {

			this.type = AgentType.STUDENT;
			 for (Building b : ContextManager.buildingContext.getRandomObjects(Building.class, 10000)) {
	                // See if the building is a bank (they will have type==2).
	                if (b.getType()==3) {
	                        this.workplace = b;
	                        break; // Have found a bank, stop searching.
	                }
	        }
			 //ContextManager.buildingContext.get
			 
		}
	}

	@Override
	public void step() throws Exception {
		
		// See what the time is, this will determine what the agent should be doing. The BigDecimal stuff
					// is just to round the time to 5 decimal places, otherwise it will never be exactly 9.0 or 17.0.
		double theTime = BigDecimal.valueOf(ContextManager.realTime).
		        round(new MathContext(5,RoundingMode.HALF_UP)).doubleValue();
		if (type == AgentType.WORKER) {
			
			GeographyWithin<DefaultAgent> geo = new GeographyWithin<DefaultAgent>(ContextManager.getAgentGeography(), 1, this);
			
			for ( DefaultAgent agente : geo.query()){
				System.out.println(agente.toString() + "/" + agente.getType() + " CERCA DE WORKER " + this.toString() + "/" + this.getType());
			}
				
			if (theTime == 8.0) { // 8am, Agent should be working
			        this.route = new Route(this, this.workplace.getCoords(), this.workplace); // Create a route to work
					
			}
			else if (theTime == 17.0) { // 5pm, agent should go home
			        this.route = new Route(this, this.home.getCoords(), this.home); // Create a route home
			}
	
			if (this.route == null) {
			        // Don't do anything if a route hasn't been created.
			} else if (this.route.atDestination()) {
			        // Have reached our destination, lets delete the old route (more efficient).
				double tiempollegada = BigDecimal.valueOf(ContextManager.realTime).
				        round(new MathContext(5,RoundingMode.HALF_UP)).doubleValue();
				LOGGER.log(Level.INFO, this.toString() + " Llegue a las " + tiempollegada);
			        this.route = null;
			}
			else {
			        // Otherwise travel towards the destination
			        this.route.travel();
			}
		} 
		else  if (type == AgentType.STUDENT){
			
			if (theTime == 21.0) { // 8am, Agent should be working
			        this.route = new Route(this, this.workplace.getCoords(), this.workplace); // Create a route to work
			}
			else if (theTime == 6.0) { // 5pm, agent should go home
			        this.route = new Route(this, this.home.getCoords(), this.home); // Create a route home
			}
	
			if (this.route == null) {
			        // Don't do anything if a route hasn't been created.
			} else if (this.route.atDestination()) {
			        // Have reached our destination, lets delete the old route (more efficient).
				double tiempollegada = BigDecimal.valueOf(ContextManager.realTime).
				        round(new MathContext(5,RoundingMode.HALF_UP)).doubleValue();
				LOGGER.log(Level.INFO, this.toString() + " Llegue a las " + tiempollegada);
			        this.route = null;
			}
			else {
			        // Otherwise travel towards the destination
			        this.route.travel();
			}
			/*if (this.route == null) {
				// route can only be null when the simulation starts, so the agent must be leaving home
				this.goingHome = false; // Choose a new building to go to
				Building b = ContextManager.buildingContext.getRandomObject();
				this.route = new Route(this, b.getCoords(), b);
			}
			if (!this.route.atDestination()) {
				this.route.travel();
			} else {
				// Have reached destination, now either go home or onto another building
				if (this.goingHome) {
					this.goingHome = false;
					Building b = ContextManager.buildingContext.getRandomObject();
					this.route = new Route(this, b.getCoords(), b);
				} else {
					this.goingHome = true;
					this.route = new Route(this, this.home.getCoords(), this.home);
				}
			}*/
			
		}

	} // step()

	/**
	 * There will be no inter-agent communication so these agents can be executed simulataneously in separate threads.
	 */
	@Override
	public final boolean isThreadable() {
		return true;
	}

	@Override
	public void setHome(Building home) {
		this.home = home;
	}

	@Override
	public Building getHome() {
		return this.home;
	}

	@Override
	public <T> void addToMemory(List<T> objects, Class<T> clazz) {
	}

	@Override
	public List<String> getTransportAvailable() {
		return null;
	}

	@Override
	public String toString() {
		return "Agent " + this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DefaultAgent))
			return false;
		DefaultAgent b = (DefaultAgent) obj;
		return this.id == b.id;
	}

	@Override
	public int hashCode() {
		return this.id;
	}
	
	public String getType(){
		return this.type.toString();
	}
	
	public AgentStatus getStatus(){
		return this.status;
	}
	
	
	
	////////////////////////////
	public boolean isInfected(){
		if (this.status == AgentStatus.INFECTED ) {
			return false;
		} else 
			return true;
	}
	

	

}
